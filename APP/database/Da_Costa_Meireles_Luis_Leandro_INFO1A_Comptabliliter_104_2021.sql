
DROP DATABASE IF EXISTS Da_Costa_Meireles_Luis_Leandro_INFO1A_Comptabliliter_104_2021;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS Da_Costa_Meireles_Luis_Leandro_INFO1A_Comptabliliter_104_2021;

-- Utilisation de cette base de donnée

USE Da_Costa_Meireles_Luis_Leandro_INFO1A_Comptabliliter_104_2021;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Da_Costa_Meireles_Luis_Leandro_INFO1A_Comptabliliter_104_2021`
--
CREATE DATABASE IF NOT EXISTS `Da_Costa_Meireles_Luis_Leandro_INFO1A_Comptabliliter_104_2021` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `Da_Costa_Meireles_Luis_Leandro_INFO1A_Comptabliliter_104_2021`;

-- --------------------------------------------------------

--
-- Table structure for table `T_Action`
--

DROP TABLE IF EXISTS `T_Action`;
CREATE TABLE `T_Action` (
  `ID` int(11) NOT NULL,
  `FK_Comptes` int(11) NOT NULL,
  `Categorie_Action` text NOT NULL,
  `Debit` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `T_Action`
--

INSERT INTO `T_Action` (`ID`, `FK_Comptes`, `Categorie_Action`, `Debit`) VALUES
(2, 9, 'Virement', 22),
(3, 12, 'Recevoir', 1000);

-- --------------------------------------------------------

--
-- Table structure for table `T_Action_Reference`
--

DROP TABLE IF EXISTS `T_Action_Reference`;
CREATE TABLE `T_Action_Reference` (
  `ID` int(11) NOT NULL,
  `FK_Action` int(11) NOT NULL,
  `FK_Reference` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `T_Action_Reference`
--

INSERT INTO `T_Action_Reference` (`ID`, `FK_Action`, `FK_Reference`) VALUES
(8, 3, 5);

-- --------------------------------------------------------

--
-- Table structure for table `T_avoir_comptes`
--

DROP TABLE IF EXISTS `T_avoir_comptes`;
CREATE TABLE `T_avoir_comptes` (
  `ID` int(11) NOT NULL,
  `FK_Personnes` int(11) NOT NULL,
  `FK_Comptes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `T_avoir_comptes`
--

INSERT INTO `T_avoir_comptes` (`ID`, `FK_Personnes`, `FK_Comptes`) VALUES
(5, 6, 9),
(6, 6, 9),
(8, 6, 9);

-- --------------------------------------------------------

--
-- Table structure for table `T_Comptes`
--

DROP TABLE IF EXISTS `T_Comptes`;
CREATE TABLE `T_Comptes` (
  `ID` int(11) NOT NULL,
  `Nom` varchar(40) NOT NULL,
  `Iban` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `T_Comptes`
--

INSERT INTO `T_Comptes` (`ID`, `Nom`, `Iban`) VALUES
(9, 'tests', 'tets'),
(10, 'Ajouter', '43214'),
(12, 'Epargne', '324141'),
(13, 'Epargne', '3241413'),
(14, 'FSA', 'DAs213');

-- --------------------------------------------------------

--
-- Table structure for table `T_Personnes`
--

DROP TABLE IF EXISTS `T_Personnes`;
CREATE TABLE `T_Personnes` (
  `ID` int(11) NOT NULL,
  `Nom` varchar(40) NOT NULL,
  `Prenom` varchar(40) NOT NULL,
  `UserName` varchar(40) NOT NULL,
  `PassWord` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `T_Personnes`
--

INSERT INTO `T_Personnes` (`ID`, `Nom`, `Prenom`, `UserName`, `PassWord`) VALUES
(4, 'test', 'luis', 'test', '5eff768ebca3ff066f442b995bb6f998d4ed7530c610d6cbbcaa2fe5c15e9e82'),
(6, 'test', 'test', 'test', '15e6ffcbfaf662f5d7ed32c57e8617f8a9f49736331ebca741e0dc940ee13d3b'),
(8, 'TestLuis', 'testlUis', 'LuisTets', '5eff768ebca3ff066f442b995bb6f998d4ed7530c610d6cbbcaa2fe5c15e9e82'),
(9, 'Nom', 'APpp', 'Test', '5eff768ebca3ff066f442b995bb6f998d4ed7530c610d6cbbcaa2fe5c15e9e82');

-- --------------------------------------------------------

--
-- Table structure for table `T_references`
--

DROP TABLE IF EXISTS `T_references`;
CREATE TABLE `T_references` (
  `ID` int(11) NOT NULL,
  `Reference` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `T_references`
--

INSERT INTO `T_references` (`ID`, `Reference`) VALUES
(2, 'allez'),
(5, 'Salaire'),
(6, 'Facture');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `T_Action`
--
ALTER TABLE `T_Action`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `T_Action_Reference`
--
ALTER TABLE `T_Action_Reference`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `T_avoir_comptes`
--
ALTER TABLE `T_avoir_comptes`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `T_Comptes`
--
ALTER TABLE `T_Comptes`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `T_Personnes`
--
ALTER TABLE `T_Personnes`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `T_references`
--
ALTER TABLE `T_references`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `T_Action`
--
ALTER TABLE `T_Action`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `T_Action_Reference`
--
ALTER TABLE `T_Action_Reference`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `T_avoir_comptes`
--
ALTER TABLE `T_avoir_comptes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `T_Comptes`
--
ALTER TABLE `T_Comptes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `T_Personnes`
--
ALTER TABLE `T_Personnes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `T_references`
--
ALTER TABLE `T_references`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
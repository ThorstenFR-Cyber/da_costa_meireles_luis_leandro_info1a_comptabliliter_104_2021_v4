"""
    Fichier : gestion_personnes_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les Personnes.
"""
import sys

import pymysql
import hashlib
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP import obj_mon_application
from APP.database.connect_db_context_manager import MaBaseDeDonnee
from APP.erreurs.exceptions import *
from APP.erreurs.msg_erreurs import *
from APP.Personnes.gestion_personnes_wtf_forms import *

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /genres_afficher
    
    Test : ex : http://127.0.0.1:5005/genres_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_genre_sel = 0 >> tous les Personnes.
                id_genre_sel = "n" affiche le genre dont l'id est "n"
"""

requestSQL = {
    "T_Comptes": "SELECT * FROM T_Comptes ",
    "T_Personnes": "SELECT * FROM `T_Personnes`",
    "T_references": "SELECT * FROM `T_references`",
    "T_Action": "SELECT T_Action.ID,T_Comptes.Nom, T_Comptes.Iban, T_Action.Categorie_Action, T_Action.Debit FROM `T_Action` INNER JOIN T_Comptes ON T_Comptes.ID = T_Action.FK_Comptes ",
    "T_avoir_comptes":"""SELECT
                            T_avoir_comptes.ID,
                            T_Personnes.Nom,
                            T_Personnes.Prenom,
                            T_Comptes.Nom,
                            T_Comptes.Iban
                        FROM
                            `T_avoir_comptes`
                        INNER JOIN
                            T_Personnes
                        ON
                            T_Personnes.ID = T_avoir_comptes.FK_Personnes
                        INNER JOIN
                            T_Comptes
                        ON
                            T_Comptes.ID = T_avoir_comptes.FK_Comptes"""
}


@obj_mon_application.route("/Afficher/<string:table>/<string:order_by>/<int:id_genre_sel>",
                           methods=['GET', 'POST'])
def genres_afficher(table, order_by, id_genre_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion Personnes ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionGenres {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_genre_sel == 0:
                    strsql_genres_afficher = requestSQL[table] + """ ORDER BY ID ASC"""
                    mc_afficher.execute(strsql_genres_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_genre"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du genre sélectionné avec un nom de variable
                    valeur_id_genre_selected_dictionnaire = {"value_id_genre_selected": id_genre_sel}
                    if table == "T_Comptes":
                        strsql_genres_afficher = requestSQL[
                                                     table] + """ WHERE T_Comptes.ID = %(value_id_genre_selected)s"""
                    elif table == "T_Action":
                        strsql_genres_afficher = requestSQL[table] + """ WHERE T_Action.ID = %(value_id_genre_selected)s"""
                    else:
                        strsql_genres_afficher = requestSQL[table] + """ WHERE ID = %(value_id_genre_selected)s"""

                    mc_afficher.execute(strsql_genres_afficher, valeur_id_genre_selected_dictionnaire)
                else:
                    strsql_genres_afficher = requestSQL[table] + """ ORDER BY ID DESC"""

                    mc_afficher.execute(strsql_genres_afficher)

                data_genres = mc_afficher.fetchall()

                print("data_genres ", data_genres, " Type : ", type(data_genres))

                # Différencier les messages si la table est vide.
                if not data_genres and id_genre_sel == 0:
                    flash("""La table '""" + table + """' est vide. !!""", "warning")
                elif not data_genres and id_genre_sel > 0:
                    # Si l'utilisateur change l'id_genre dans l'URL et que le genre n'existe pas,
                    flash(f"La donnée demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_genre" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"La donnée demandé est affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. genres_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} genres_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    data_row = data_genres[0]
    print("data_row ", data_row, " Type : ", type(data_row))
    # Envoie la page "HTML" au serveur.
    return render_template("Personnes/genres_afficher.html", data=data_genres, rowData=data_row, table=table)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /genres_ajouter
    
    Test : ex : http://127.0.0.1:5005/genres_ajouter
    
    Paramètres : sans
    
    But : Ajouter un genre pour un film
    
    Remarque :  Dans le champ "name_genre_html" du formulaire "Personnes/genres_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""

requestINSERTSQL = {
    "Comptes": " INSERT INTO `T_Comptes`(`ID`, `Nom`, `Iban`) VALUES (NULL ,%(value_Name)s,%(Iban_Compte)s)",

    "Personnes": "INSERT INTO `T_Personnes`(`ID`, `Nom`, `Prenom`, `UserName`, `PassWord`) VALUES (NULL , %(value_Name)s, "
                 "%(value_Prenom)s, %(value_Username)s, %(value_Password)s) ",

    "references": " INSERT INTO `T_references`(`ID`, `Reference` ) VALUES (NULL , "
                  "%(value_Reference)s)",

    "Action": "INSERT INTO `T_Action`(`ID`, `FK_Comptes`, Categorie_Action, Debit ) VALUES (NULL , "
              "%(value_compte_action)s, %(value_type_action)s,%(value_nom_Action)s)",

    "avoir_comptes": """INSERT INTO `T_avoir_comptes` (`ID`, `FK_Personnes`, `FK_Comptes`) VALUES (NULL, %(value_Personnes_avoirCompte)s, %(value_compte_avoirCompte)s);"""
}


@obj_mon_application.route("/Ajouter/<string:table>", methods=['GET', 'POST'])
def genres_ajouter_wtf(table):
    with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
        strsql_genres_afficher = requestSQL[table] + """ ORDER BY ID ASC"""
        mc_afficher.execute(strsql_genres_afficher)

    data_genres = mc_afficher.fetchall()
    if table == "T_Action" or table == "T_avoir_comptes":
        with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
            strsql_genres_afficher = """SELECT * FROM T_Comptes """
            mc_afficher.execute(strsql_genres_afficher)
            data = mc_afficher.fetchall()
            print(data)
            groups_list_Comptes = [(i['ID'], i['Nom'] + " " + i['Iban']) for i in data]

    if table == "T_avoir_comptes":
        with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
            strsql_genres_afficher = """SELECT * FROM T_Personnes """
            mc_afficher.execute(strsql_genres_afficher)
            data = mc_afficher.fetchall()
            print(data)
            groups_list_Personnes = [(i['ID'], i['Nom'] + " " + i['Prenom']) for i in data]

    if table == "T_Personnes":
        form = FormWTFAjouterPersonnes()
    elif table == "T_Comptes":
        form = FormWTFAjouterComptes()
    elif table == "T_references":
        form = FormWTFAjouterReference()
    elif table == "T_Action":
        form = FormWTFAjouterAction()
        form.Compte_action_wtf.choices = groups_list_Comptes
    elif table == "T_avoir_comptes":
        form = FormWTFAjouterAvoirCompte()
        form.Comptes_avoirCompte_wtf.choices = groups_list_Comptes
        form.Personne_avoirCompte_wtf.choices = groups_list_Personnes

    table = table.replace('T_', '')

    if request.method == "POST":
        print("Valide Poste")

        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion Personnes ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionGenres {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if request.method == "POST" or form.validate_on_submit():
                if table == "Personnes":
                    name_Personnes = form.Nom_Personnes_wtf.data
                    Prenom_Personnes = form.Prenom_Personnes_wtf.data
                    Username_Personnes = form.UserName_Personnes_wtf.data
                    PasswordClear = form.PassWord_Personnes_wtf.data
                    print("1")
                    Password_Personnes = hashlib.sha256(PasswordClear.encode('ascii')).hexdigest()
                    print("2")
                    print(Password_Personnes)
                    valeurs_insertion_dictionnaire = {"value_Name": name_Personnes, "value_Prenom": Prenom_Personnes,
                                                      "value_Username": Username_Personnes,
                                                      "value_Password": Password_Personnes}
                    print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                elif table == "Comptes":
                    Nom_Comptes = form.Nom_Comptes_wtf.data
                    Iban_Comptes = form.Iban_Comptes_wtf.data

                    valeurs_insertion_dictionnaire = {"value_Name": Nom_Comptes,
                                                      "Iban_Compte": Iban_Comptes}
                    print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                elif table == "references":
                    Nom_Reference = form.nom_Reference_wtf.data

                    valeurs_insertion_dictionnaire = {"value_Reference": Nom_Reference}
                    print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                elif table == "Action":
                    Nom_Action = form.nom_Action_wtf.data
                    type_Action = form.type_action_wtf.data
                    compte = form.Compte_action_wtf.data

                    valeurs_insertion_dictionnaire = {"value_nom_Action": Nom_Action, "value_type_action": type_Action,
                                                      "value_compte_action": compte}
                    print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                elif table == "avoir_comptes":
                    Personnes = form.Personne_avoirCompte_wtf.data
                    Compte = form.Comptes_avoirCompte_wtf.data


                    valeurs_insertion_dictionnaire = {"value_Personnes_avoirCompte": Personnes, "value_compte_avoirCompte": Compte}
                    print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_genre = requestINSERTSQL[table]

                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_genre, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('genres_afficher', table="T_" + table, order_by='DESC', id_genre_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_genre_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_genre_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion Personnes CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("Personnes/genres_ajouter_wtf.html", form=form, table=table, data=data_genres[0])


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /genre_update
    
    Test : ex cliquer sur le menu "Personnes" puis cliquer sur le bouton "EDIT" d'un "genre"
    
    Paramètres : sans
    
    But : Editer(update) un genre qui a été sélectionné dans le formulaire "genres_afficher.html"
    
    Remarque :  Dans le champ "nom_genre_update_wtf" du formulaire "Personnes/genre_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""

requestUpdateSelecteSQL = {
    "T_Comptes": "SELECT * FROM T_Comptes",
    "T_Personnes": "SELECT * FROM `T_Personnes`",
    "T_references": "SELECT * FROM `T_references`",
    "T_Action": "SELECT * FROM `T_Action`",
}

requestUpdateSQL = {
    "T_Comptes": "UPDATE T_Comptes SET Nom = %(value_Nom_Comptes)s, Iban = %(value_Iban_Comptes)s ",
    "T_Personnes": "UPDATE `T_Personnes` SET `Nom`= %(value_Nom_Personnes)s,`Prenom`= %(value_Prenom_Personnes)s,`UserName`= %(value_UserName_Personnes)s,`PassWord`= %(value_PassWord_Personnes)s",
    "T_references": "UPDATE T_references SET  `Reference` = %(value_Nom_Reference)s",
    "T_Action": "UPDATE T_Action SET  `Categorie_Action` = %(value_type_Action)s, Debit = %(value_debit_Action)s, FK_Comptes = %(value_Compte_Action)s "
}


@obj_mon_application.route("/Update/<string:table>", methods=['GET', 'POST'])
def genre_update_wtf(table):
    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_genre"
    id_genre_update = request.values['id_genre_btn_edit_html']
    print(id_genre_update)
    print(table)
    if table == "T_Action" or table == "T_avoir_comptes":
        with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
            strsql_genres_afficher = """SELECT * FROM T_Comptes"""
            mc_afficher.execute(strsql_genres_afficher)
            data = mc_afficher.fetchall()
            print(data)
            groups_list_Comptes = [(i['ID'], i['Nom'] + " " + i['Iban']) for i in data]

    if table == "T_avoir_comptes":
        with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
            strsql_genres_afficher = """SELECT * FROM T_Personnes"""
            mc_afficher.execute(strsql_genres_afficher)
            data = mc_afficher.fetchall()
            print(data)
            groups_list_Personnes = [(i['ID'], i['Nom'] + " " + i['Prenom']) for i in data]


    # Objet formulaire pour l'UPDATE
    if table == "T_Comptes":
        form_update = FormWTFUpdateComptes()
    elif table == "T_Personnes":
        form_update = FormWTFUpdatePersonnes()
    elif table == "T_references":
        form_update = FormWTFUpdateReferences()
        print("select references update")
    elif table == "T_Action":
        form_update = FormWTFUpdateAction()
    elif table == "T_avoir_comptes":
        form_update = FormWTFUpdateAvoirCompte()



    try:

        if request.method == "POST":
            # Récupèrer la valeur du champ depuis "genre_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            print("in post")
            if table == "T_references":
                value_Nom_Reference = form_update.nom_Reference_wtf.data
                value_Nom_Reference = value_Nom_Reference.lower()
                valeur_update_dictionnaire = {"value_id_genre": id_genre_update,
                                              "value_Nom_Reference": value_Nom_Reference}

                print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            elif table == "T_Comptes":
                value_Nom_Comptes = form_update.Nom_Comptes_wtf.data
                value_Nom_Comptes = value_Nom_Comptes.lower()
                value_Type_Comptes = form_update.Type_Comptes_wtf.data
                value_Type_Comptes = value_Type_Comptes.lower()
                value_Iban_Comptes = form_update.Iban_Comptes_wtf.data
                value_Iban_Comptes = value_Iban_Comptes.lower()
                valeur_update_dictionnaire = {"value_id_genre": id_genre_update, "value_Nom_Comptes": value_Nom_Comptes,
                                              "value_Type_Comptes": value_Type_Comptes,
                                              "value_Iban_Comptes": value_Iban_Comptes}
                print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            elif table == "T_Personnes":
                value_Nom_Personnes = form_update.Nom_Personnes_wtf.data
                value_Nom_Personnes = value_Nom_Personnes.lower()
                value_Prenom_Personnes = form_update.Prenom_Personnes_wtf.data
                value_Prenom_Personnes = value_Prenom_Personnes.lower()
                value_UserName_Personnes = form_update.UserName_Personnes_wtf.data
                value_UserName_Personnes = value_UserName_Personnes.lower()
                value_PassWord_Personnes = form_update.PassWord_Personnes_wtf.data
                value_PassWord_Personnes = hashlib.sha256(value_PassWord_Personnes.encode('ascii')).hexdigest()

                valeur_update_dictionnaire = {"value_id_genre": id_genre_update,
                                              "value_Nom_Personnes": value_Nom_Personnes,
                                              "value_Prenom_Personnes": value_Prenom_Personnes,
                                              "value_UserName_Personnes": value_UserName_Personnes,
                                              "value_PassWord_Personnes": value_PassWord_Personnes}
                print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            elif table == "T_Action":
                print("Update Action")
                Nom_Action = form_update.nom_Action_wtf.data
                type_Action = form_update.type_action_wtf.data
                compte = form_update.Compte_action_wtf.data

                valeur_update_dictionnaire = {"value_id_genre": id_genre_update,
                                              "value_debit_Action": Nom_Action,
                                              "value_type_Action": type_Action,
                                              "value_Compte_Action": compte}
                print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intitulegenre = requestUpdateSQL[table] + """ WHERE ID = %(value_id_genre)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulegenre, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_genre_update"
            return redirect(url_for('genres_afficher', table=table, order_by="ASC", id_genre_sel=id_genre_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_genre" et "intitule_genre" de la "t_genre"
            str_sql_id_genre = requestUpdateSelecteSQL[table] + " WHERE ID = %(value_id_genre)s"
            valeur_select_dictionnaire = {"value_id_genre": id_genre_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_genre, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom genre" pour l'UPDATE
            data_nom_genre = mybd_curseur.fetchone()
            print("data_nom_genre ", data_nom_genre, " type ", type(data_nom_genre), " genre ",
                  data_nom_genre['ID'])

            # Afficher la valeur sélectionnée dans le champ du formulaire "genre_update_wtf.html"
            if table == "T_Comptes":
                form_update.Nom_Comptes_wtf.data = data_nom_genre["Nom"]
                form_update.Iban_Comptes_wtf.data = data_nom_genre["Iban"]
            elif table == "T_references":
                form_update.nom_Reference_wtf.data = data_nom_genre["Reference"]
            elif table == "T_Personnes":
                form_update.Nom_Personnes_wtf.data = data_nom_genre["Nom"]
                form_update.Prenom_Personnes_wtf.data = data_nom_genre["Prenom"]
                form_update.UserName_Personnes_wtf.data = data_nom_genre["UserName"]
                form_update.PassWord_Personnes_wtf.data = data_nom_genre["PassWord"]
            elif table == "T_Action":
                print("test Data")
                form_update.nom_Action_wtf.data = data_nom_genre["Debit"]
                form_update.Compte_action_wtf.choices = groups_list_Comptes
                form_update.type_action_wtf.default = data_nom_genre["Categorie_Action"]
                form_update.Compte_action_wtf.default = data_nom_genre["FK_Comptes"]
            elif table == "T_avoir_Comptes":
                form_update.Comptes_avoirCompte_wtf.choices = groups_list_Comptes
                form_update.Personne_avoirCompte_wtf.choices = groups_list_Personnes




    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans genre_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans genre_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans genre_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans genre_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("Personnes/genre_update_wtf.html", form_update=form_update, table=table)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /genre_delete
    
    Test : ex. cliquer sur le menu "Personnes" puis cliquer sur le bouton "DELETE" d'un "genre"
    
    Paramètres : sans
    
    But : Effacer(delete) un genre qui a été sélectionné dans le formulaire "genres_afficher.html"
    
    Remarque :  Dans le champ "nom_genre_delete_wtf" du formulaire "Personnes/genre_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""
requetSelDeletSQL = {
    "T_Comptes": "SELECT * FROM T_Personnes WHERE ID = %(value_id_genre)s",
    "T_Personnes": "SELECT T_Personnes.Nom, T_Personnes.Prenom, T_Comptes.Nom, T_Comptes.Iban FROM `T_avoir_comptes` INNER JOIN T_Personnes ON T_Personnes.ID = FK_Personnes INNER JOIN T_Comptes ON T_Comptes.ID = FK_Comptes WHERE T_Personnes.ID = %(value_id_genre)s ",
    "T_references": "SELECT T_references.Reference, T_Action.Categorie_Action, T_Action.Debit FROM `T_Action_Reference` INNER JOIN T_Action ON T_Action.ID = FK_Action INNER JOIN T_references ON T_references.ID = FK_Reference WHERE T_references.ID = %(value_id_genre)s",
    "T_Action": "SELECT * FROM T_Comptes WHERE ID = %(value_id_genre)s",
    "T_avoir_comptes": """SELECT
                                T_Personnes.ID,
                                T_Personnes.Nom,
                                T_Personnes.Prenom
                            FROM
                                `T_avoir_comptes`
                            INNER JOIN T_Personnes ON T_Personnes.ID = T_avoir_comptes.FK_Personnes
                            WHERE
                                T_avoir_comptes.ID = %(value_id_genre)s"""
}

requetDeletSQL = {
    "T_Personnes": "DELETE FROM `T_Personnes` WHERE ID = %(value_id_genre)s ; DELETE FROM `T_avoir_comptes` WHERE FK_Personnes = %(value_id_genre)s ; ",
    "T_Comptes": "DELETE FROM T_Comptes WHERE ID = %(value_id_genre)s; DELETE FROM `T_avoir_comptes` WHERE FK_Comptes = %(value_id_genre)s",
    "T_references": "DELETE FROM T_references WHERE ID = %(value_id_genre)s; DELETE FROM T_Action_Reference WHERE FK_Reference = %(value_id_genre)s;",
    "T_Action": "DELETE FROM T_Action WHERE  ID = %(value_id_genre)s",
    "T_avoir_comptes": "DELETE FROM `T_avoir_comptes` WHERE `ID` = %(value_id_genre)s",
}


@obj_mon_application.route("/Delete/<string:table>", methods=['GET', 'POST'])
def genre_delete_wtf(table):
    data_films_attribue_genre_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_genre"
    id_genre_delete = request.values['id_genre_btn_delete_html']
    print(table)
    # Objet formulaire pour effacer le genre sélectionné.
    form_delete = FormWTFDeleteGenre()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("genres_afficher", table=table, order_by="ASC", id_genre_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "Personnes/genre_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_films_attribue_genre_delete = session['data_films_attribue_genre_delete']
                print("data_films_attribue_genre_delete ", data_films_attribue_genre_delete)

                flash(f"Effacer le genre de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer genre" qui va irrémédiablement EFFACER le genre
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_genre": id_genre_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_films_genre = requetDeletSQL[table]
                # Manière brutale d'effacer d'abord la "fk_genre", même si elle n'existe pas dans la "t_genre_film"
                # Ensuite on peut effacer le genre vu qu'il n'est plus "lié" (INNODB) dans la "t_genre_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_films_genre, valeur_delete_dictionnaire)

                flash(f"Genre définitivement effacé !!", "success")
                print(f"Genre définitivement effacé !!")

                # afficher les données
                return redirect(url_for('genres_afficher', table=table, order_by="ASC", id_genre_sel=0))

        if request.method == "GET":

            valeur_select_dictionnaire = {"value_id_genre": id_genre_delete}
            print(id_genre_delete, type(id_genre_delete))
            # Requête qui affiche tous les Comptes qui ont le genre que l'utilisateur veut effacer
            str_sql_genres_films_delete = requetSelDeletSQL[table]

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_genres_films_delete, valeur_select_dictionnaire)
            data_films_attribue_genre_delete = mybd_curseur.fetchall()

            print("data_films_attribue_genre_delete...", data_films_attribue_genre_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "Personnes/genre_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_films_attribue_genre_delete'] = data_films_attribue_genre_delete

            # Opération sur la BD pour récupérer "id_genre" et "intitule_genre" de la "t_genre"
            str_sql_id_genre = requestSQL[table]

            mybd_curseur.execute(str_sql_id_genre, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom genre" pour l'action DELETE
            data_nom_genre = mybd_curseur.fetchone()
            print("data: ", data_nom_genre, " type ", type(data_nom_genre))

            # Afficher la valeur sélectionnée dans le champ du formulaire "genre_delete_wtf.html"
            if table == "T_Personnes":
                form_delete.nom_genre_delete_wtf.data = data_nom_genre["Nom"] + " " + data_nom_genre["Prenom"]
            elif table == "T_Comptes":
                form_delete.nom_genre_delete_wtf.data = data_nom_genre["Nom"] + " " + data_nom_genre["Iban"]
            elif table == "T_references":
                form_delete.nom_genre_delete_wtf.data = data_nom_genre["Reference"]
            elif table == "T_Action":
                form_delete.nom_genre_delete_wtf.data = data_nom_genre['Nom'] + " " + data_nom_genre["Categorie_Action"]
            elif table == "T_avoir_comptes":
                form_delete.nom_genre_delete_wtf.data = data_nom_genre['Nom'] + " " + data_nom_genre["Prenom"] + " " + data_nom_genre['Iban']


            # Le bouton pour l'action "DELETE" dans le form. "genre_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans genre_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans genre_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans genre_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans genre_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("Personnes/genre_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_films_associes=data_films_attribue_genre_delete,
                           table = table
                           )

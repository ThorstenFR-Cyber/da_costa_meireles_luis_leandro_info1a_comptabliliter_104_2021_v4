"""
    Fichier : gestion_personnes_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import *
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterPersonnes(FlaskForm):
    """
        Dans le formulaire "genres_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    Nom_Personnes_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    Nom_Personnes_wtf = StringField("Inserter le nom ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                    Regexp(Nom_Personnes_regexp,
                                                                           message="Pas de chiffres, de caractères "
                                                                                   "spéciaux, "
                                                                                   "d'espace à double, de double "
                                                                                   "apostrophe, de double trait union")
                                                                    ])

    Prenom_Personnes_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    Prenom_Personnes_wtf = StringField("Inserter le Prénom ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                          Regexp(Prenom_Personnes_regexp,
                                                                                 message="Pas de chiffres, de caractères "
                                                                                         "spéciaux, "
                                                                                         "d'espace à double, de double "
                                                                                         "apostrophe, de double trait union")
                                                                          ])
    UserName_Personnes_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    UserName_Personnes_wtf = StringField("Inserter le Username ",
                                         validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                     Regexp(UserName_Personnes_regexp,
                                                            message="Pas de chiffres, de caractères "
                                                                    "spéciaux, "
                                                                    "d'espace à double, de double "
                                                                    "apostrophe, de double trait union")
                                                     ])
    PassWord_Personnes_regexp = ""
    PassWord_Personnes_wtf = PasswordField("Inserter le Password ",
                                           validators=[Length(min=2, max=200, message="min 2 max 20"),
                                                       Regexp(PassWord_Personnes_regexp,
                                                              message="min 2 caractères, max 200 caractères")
                                                       ])
    submit = SubmitField("Enregistrer la nouvelle personne")


class FormWTFAjouterComptes(FlaskForm):
    """
        Dans le formulaire "genres_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    Nom_Comptes_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    Nom_Comptes_wtf = StringField("Inserter le nom du compte ",
                                  validators=[Length(min=2, max=20, message="min 2 max 20"),
                                              Regexp(Nom_Comptes_regexp,
                                                     message="Pas de chiffres, de caractères "
                                                             "spéciaux, "
                                                             "d'espace à double, de double "
                                                             "apostrophe, de double trait union")
                                              ])

    Iban_Comptes_regexp = "^[A-Za-z0-9]*$"
    Iban_Comptes_wtf = StringField("Inserer l'iban du compte ",
                                   validators=[Length(min=2, max=20, message="min 2 max 20"),
                                               Regexp(Iban_Comptes_regexp,
                                                      message="Pas de chiffres, de caractères "
                                                              "spéciaux, "
                                                              "d'espace à double, de double "
                                                              "apostrophe, de double trait union")
                                               ])

    submit = SubmitField("Enregistrer le nouveaux compte")

class FormWTFAjouterAvoirCompte(FlaskForm):
    """
        Dans le formulaire "genres_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    Personne_avoirCompte_wtf = SelectField("Selectionner la personnes",choices=[])

    Comptes_avoirCompte_wtf = SelectField("Selectionner le compte", choices=[])

    submit = SubmitField("Enregistrer la liaison")

class FormWTFAjouterAction(FlaskForm):
    """
        Dans le formulaire "genres_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """

    nom_Action_regexp = "^[0-9]*$"
    nom_Action_wtf = DecimalField("Inserer la somme",
                                  validators=[
                                      Regexp(nom_Action_regexp,
                                             message="Pas de chiffres, de caractères "
                                                     "spéciaux, "
                                                     "d'espace à double, de double "
                                                     "apostrophe, de double trait union")
                                  ])

    type_action_wtf = SelectField("Selectionner l'action",
                                  choices=[('Virement', 'Virement'), ('Recevoir', 'Reçevoire')])

    Compte_action_wtf = SelectField("Selectionner le compte", choices=[])

    submit = SubmitField("Enregistrer  l'action")


class FormWTFAjouterReference(FlaskForm):
    """
        Dans le formulaire "genres_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """

    nom_Reference_regexp = "^[A-Za-z]*$"
    nom_Reference_wtf = StringField("Inserer la nouvelle réference ",
                                    validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                Regexp(nom_Reference_regexp,
                                                       message="Pas de chiffres, de caractères "
                                                               "spéciaux, "
                                                               "d'espace à double, de double "
                                                               "apostrophe, de double trait union")
                                                ])

    submit = SubmitField("Enregistrer une nouvelle référence")
class FormWTFUpdateAvoirCompte(FlaskForm):
    """
        Dans le formulaire "genres_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    Personne_avoirCompte_wtf = SelectField("Selectionner la personnes",choices=[])

    Comptes_avoirCompte_wtf = SelectField("Selectionner le compte", choices=[])

    submit = SubmitField("Mettre à jour")

class FormWTFUpdateComptes(FlaskForm):
    """
        Dans le formulaire "genre_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    Nom_Comptes_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    Nom_Comptes_wtf = StringField("Inserter le nom du compte ",
                                  validators=[Length(min=2, max=20, message="min 2 max 20"),
                                              Regexp(Nom_Comptes_regexp,
                                                     message="Pas de chiffres, de caractères "
                                                             "spéciaux, "
                                                             "d'espace à double, de double "
                                                             "apostrophe, de double trait union")
                                              ])

    Type_Comptes_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    Type_Comptes_wtf = StringField("Inserter le type de compte ",
                                   validators=[Length(min=2, max=20, message="min 2 max 20"),
                                               Regexp(Type_Comptes_regexp,
                                                      message="Pas de chiffres, de caractères "
                                                              "spéciaux, "
                                                              "d'espace à double, de double "
                                                              "apostrophe, de double trait union")
                                               ])
    Iban_Comptes_regexp = "^[A-Za-z0-9]*$"
    Iban_Comptes_wtf = StringField("Inserer l'iban du compte ",
                                   validators=[Regexp(Iban_Comptes_regexp,
                                                      message="Pas de chiffres, de caractères "
                                                              "spéciaux, "
                                                              "d'espace à double, de double "
                                                              "apostrophe, de double trait union")
                                               ])

    submit = SubmitField("Update le Comptes")


class FormWTFUpdatePersonnes(FlaskForm):
    """
        Dans le formulaire "genres_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    Nom_Personnes_regexp = "[A-Z a-z]"
    Nom_Personnes_wtf = StringField("Inserter le nom ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                    Regexp(Nom_Personnes_regexp,
                                                                           message="Pas de chiffres, de caractères "
                                                                                   "spéciaux, "
                                                                                   "d'espace à double, de double "
                                                                                   "apostrophe, de double trait union")
                                                                    ])

    Prenom_Personnes_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    Prenom_Personnes_wtf = StringField("Inserter le Prénom ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                          Regexp(Prenom_Personnes_regexp,
                                                                                 message="Pas de chiffres, de caractères "
                                                                                         "spéciaux, "
                                                                                         "d'espace à double, de double "
                                                                                         "apostrophe, de double trait union")
                                                                          ])
    UserName_Personnes_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    UserName_Personnes_wtf = StringField("Inserter le Username ",
                                         validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                     Regexp(UserName_Personnes_regexp,
                                                            message="Pas de chiffres, de caractères "
                                                                    "spéciaux, "
                                                                    "d'espace à double, de double "
                                                                    "apostrophe, de double trait union")
                                                     ])
    PassWord_Personnes_regexp = ""
    PassWord_Personnes_wtf = PasswordField("Inserter le Password ",
                                           validators=[Length(min=2, max=200, message="min 2 max 20"),
                                                       Regexp(PassWord_Personnes_regexp,
                                                              message="min 2 caractères, max 200 caractères")
                                                       ])
    submit = SubmitField("Modifer la personne")


class FormWTFUpdateReferences(FlaskForm):
    """
        Dans le formulaire "genres_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_Reference_regexp = "^[A-Za-z]*$"
    nom_Reference_wtf = StringField("Inserer la réference ",
                                    validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                Regexp(nom_Reference_regexp,
                                                       message="Pas de chiffres, de caractères "
                                                               "spéciaux, "
                                                               "d'espace à double, de double "
                                                               "apostrophe, de double trait union")
                                                ])

    submit = SubmitField("Modifer la reference")


class FormWTFUpdateAction(FlaskForm):
    """
        Dans le formulaire "genres_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """

    nom_Action_regexp = "^[0-9]*$"
    nom_Action_wtf = DecimalField("Inserer la somme",
                                  validators=[
                                      Regexp(nom_Action_regexp,
                                             message="Pas de chiffres, de caractères "
                                                     "spéciaux, "
                                                     "d'espace à double, de double "
                                                     "apostrophe, de double trait union")
                                  ])

    type_action_wtf = SelectField("Selectionner l'action",
                                  choices=[('Virement', 'Virement'), ('Recevoir', 'Reçevoire')])

    Compte_action_wtf = SelectField("Selectionner le compte", choices=[])

    submit = SubmitField("Modifier une action")


class FormWTFDeleteGenre(FlaskForm):
    """
        Dans le formulaire "genre_delete_wtf.html"

        nom_genre_delete_wtf : Champ qui reçoit la valeur du genre, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "genre".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_genre".
    """
    nom_genre_delete_wtf = StringField("Effacer cette donnée")
    submit_btn_del = SubmitField("Effacer")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
